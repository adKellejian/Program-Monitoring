"""
Sample Gevent application with Rollbar integration.
"""
import sys
import logging

from gevent.pywsgi import WSGIServer
import rollbar
import webob

# configure logging so that rollbar's log messages will appear
logging.basicConfig()

def application(environ, start_response):
    request = webob.Request(environ)
    status = '200 OK'
    headers = [('Content-Type', 'text/html')]
    start_response(status, headers)

    yield '<p>Hello world</p>'

    # extra fields we'd like to send along to rollbar (optional)
    extra_data = {'datacenter': 'us1', 'app' : {'version': '1.1'}}

    try:
        # will raise a NameError about 'bar' not being defined
        foo = bar
    except:
        # report full exception info
        rollbar.report_exc_info(sys.exc_info(), request, extra_data=extra_data)

        # and/or, just send a string message with a level
        rollbar.report_message("Here's a message", 'info', request, extra_data=extra_data)

        yield '<p>Caught an exception</p>'

# initialize rollbar with an access token and environment name
rollbar.init('', 'development')

# now start the wsgi server
WSGIServer(('', 8000), application).serve_forever()

.